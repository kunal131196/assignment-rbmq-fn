#!/bin/bash
set -e

if [ "$1" = 'worker' ]; then
 
  python -u worker.py
elif [ "$1" = 'publish' ]; then
  python -u publisher.py $2
else
  echo "Nothing to do!"
fi
