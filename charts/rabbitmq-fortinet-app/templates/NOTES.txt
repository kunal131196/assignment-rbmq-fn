To inject sample events, run the following:

kubectl run publish -it --rm --image=kunalborse/python-app --restart=Never publish <number of messages>
